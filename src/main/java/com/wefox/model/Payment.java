package com.wefox.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "payments")
public class Payment {
    @Id
    @Column(name = "payment_id")
    @JsonProperty("payment_id")
    private String paymentId;

    @JsonProperty("account_id")
    @Column(name = "account_id")
    private Integer accountId;

    @JsonProperty("payment_type")
    @Column(name = "payment_type")
    private String paymentType;

    @JsonProperty("credit_card")
    @Column(name = "credit_card")
    private String creditCard;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("delay")
    @Transient
    private String delay;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
