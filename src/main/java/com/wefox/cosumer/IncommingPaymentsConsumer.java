package com.wefox.cosumer;

public interface IncommingPaymentsConsumer {

    public abstract void processOnlineMessage(String content);

    public abstract void processOfflineMessage(String content);

}