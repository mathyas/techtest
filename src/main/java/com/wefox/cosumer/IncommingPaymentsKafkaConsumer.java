package com.wefox.cosumer;

import static com.wefox.remote.ErrorType.DATABASE;
import static com.wefox.remote.ErrorType.NETWORK;
import static com.wefox.remote.ErrorType.OTHER;
import static java.lang.System.out;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wefox.model.Payment;
import com.wefox.remote.Error;
import com.wefox.remote.InvalidPayment;
import com.wefox.remote.PaymentValidator;
import com.wefox.remote.RemoteLogClient;
import com.wefox.service.PaymentService;

@Component
public class IncommingPaymentsKafkaConsumer implements IncommingPaymentsConsumer {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentValidator paymentValidator;

    @Autowired
    private RemoteLogClient logClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    @KafkaListener(topics = "online", groupId = "test-consumer-group")
    public void processOnlineMessage(String content) {
        System.out.println("Message received: " + content);
        Payment payment = null;
        try {
            payment = objectMapper.readValue(content, Payment.class);
            paymentValidator.validate(payment);
            paymentService.addPayment(payment);
        } catch (JsonProcessingException e) {
            logClient.log(new Error(payment != null ? payment.getPaymentId() : "-", OTHER, e.getMessage()));
        } catch (HibernateException | DataIntegrityViolationException e) {
            logClient.log(new Error(payment.getPaymentId(), DATABASE, e.getMessage()));
        } catch (InvalidPayment e) {
            logClient.log(new Error(payment.getPaymentId(), OTHER, e.getMessage()));
        } catch (RestClientException e) {
            logClient.log(new Error(payment.getPaymentId(), NETWORK, e.getMessage()));
        }
        System.out.println("Message processed: " + content);
    }

    @Override
    @KafkaListener(topics = "offline", groupId = "test-consumer-group")
    public void processOfflineMessage(String content) {
        out.println("Message received: " + content);
        Payment payment = null;
        try {
            payment = objectMapper.readValue(content, Payment.class);
            paymentService.addPayment(payment);
        } catch (JsonProcessingException e) {
            logClient.log(new Error(payment != null ? payment.getPaymentId() : "-", OTHER, e
                    .getMessage()));
        } catch (HibernateException | DataIntegrityViolationException e) {
            logClient.log(new Error(payment.getPaymentId(), DATABASE, e.getMessage()));
        }
        out.println("Message processed: " + content);
    }
}
