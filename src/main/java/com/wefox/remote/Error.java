package com.wefox.remote;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
    @JsonProperty("payment_id")
    private String paymentId;
    @JsonProperty("error_type")
    private ErrorType errorType;
    @JsonProperty("error_description")
    private String errorDescription;

    private Error() {};
    
    public Error(String paymentId, ErrorType errorType, String errorDescription) {
        this.paymentId = paymentId;
        this.errorType = errorType;
        this.errorDescription = errorDescription;
    }
}
