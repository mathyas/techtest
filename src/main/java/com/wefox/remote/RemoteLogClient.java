package com.wefox.remote;

import static java.lang.System.err;
import static org.springframework.http.HttpMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class RemoteLogClient implements LogClient {
    private String url = "http://localhost:9000/log";
    
    @Autowired
    private RestTemplate restTemplate;
    
    public void log(Error error) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(new MediaType("application", "json"));
        HttpEntity<Error> requestEntity = new HttpEntity<Error>(error, requestHeaders);
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, POST,
                    requestEntity, String.class);
        } catch (RestClientException e) {
            err.println("Cannot call Error LOG Service.");
        }

    }

}
