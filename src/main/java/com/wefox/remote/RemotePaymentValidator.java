package com.wefox.remote;

import static org.springframework.http.HttpMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wefox.model.Payment;

@Component
public class RemotePaymentValidator implements PaymentValidator {

    private String url = "http://localhost:9000/payment";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void validate(Payment payment) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(new MediaType("application", "json"));
        HttpEntity<Payment> requestEntity = new HttpEntity<Payment>(payment, requestHeaders);
        ResponseEntity<String> response = restTemplate.exchange(url, POST,
                requestEntity, String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new InvalidPayment("Error on payment validation. Error code: " + response.getStatusCode().value());
        }
    }

}
