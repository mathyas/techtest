package com.wefox.remote;

public enum ErrorType {
    DATABASE("database"), NETWORK("network"), OTHER("other");
    
    private String description;
    private ErrorType(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
