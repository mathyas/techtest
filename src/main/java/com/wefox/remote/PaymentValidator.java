package com.wefox.remote;

import com.wefox.model.Payment;

public interface PaymentValidator {

    public void validate(Payment payment);

}