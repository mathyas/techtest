package com.wefox.remote;

public class InvalidPayment extends RuntimeException {

    private static final long serialVersionUID = -7999539735157988836L;

    public InvalidPayment(String message) {
        super(message);
    }
}
