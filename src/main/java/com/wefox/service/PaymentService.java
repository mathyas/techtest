package com.wefox.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wefox.dao.AccountRepository;
import com.wefox.dao.PaymentRepository;
import com.wefox.model.Account;
import com.wefox.model.Payment;

@Service
public class PaymentService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    public void addPayment(Payment payment) {
        paymentRepository.addPayment(payment);
        Account account = accountRepository.getAccount(payment.getAccountId());
        account.setLastPaymentDate(new Date());
        accountRepository.updateAccount(account);
    }
}
