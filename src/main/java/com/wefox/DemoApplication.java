package com.wefox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.wefox.config.DatabaseConfiguration;
import com.wefox.config.RemoteConfiguration;

@SpringBootApplication
@Import({ DatabaseConfiguration.class, RemoteConfiguration.class, })
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}