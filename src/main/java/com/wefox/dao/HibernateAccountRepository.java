package com.wefox.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wefox.model.Account;

@Repository
@Transactional
public class HibernateAccountRepository implements AccountRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public Account getAccount(Integer id) {
        Session session = this.sessionFactory.getCurrentSession();
        Account account = (Account) session.get(Account.class, id);
        return account;
    }
    
    public void updateAccount(Account account) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(account);
    }
}
