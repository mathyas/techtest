package com.wefox.dao;

import com.wefox.model.Payment;

public interface PaymentRepository { 
    public Payment addPayment(Payment payment);
}