package com.wefox.dao;

import com.wefox.model.Account;


public interface AccountRepository {
    public Account getAccount(Integer id);
    
    public void updateAccount(Account account);
}
