package com.wefox.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wefox.model.Payment;

@Repository
@Transactional
public class HibernatePaymentRepository implements PaymentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Payment addPayment(Payment payment) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(payment);
        return payment;
    }

}
